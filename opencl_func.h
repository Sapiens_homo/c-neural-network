#ifndef __OCL_FUNC_H__
#define __OCL_FUNC_H__
#define CL_TARGET_OPENCL_VERSION 300
#include<limits.h>
#include <CL/cl.h>
#include<CL/opencl.h>
#include <CL/cl_icd.h>
#include <CL/cl_platform.h>
#include<CL/cl_version.h>
#include"neural.h"
void cl_network(Neural_network net);
void cl_init(void);
#endif
