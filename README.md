# c-neural-network  
### Stock Price Predict with Recurrent Neural Network
##### *use Python and C with OpenCL*
---
**TODO**:
- OpenCL
- debug the network or rewrite it

The structture of our project:
| **FIle name** | **Use** |
| :-------: | :---: |
| main.c | main program |
| database.h | Read/Write functions |
| neural.h | classes of Neuraw Networks |
| fetch\_history.py | Get history of stocks and push them into stock-\<code\>|
| database-\<code\> | store history of stock \<code\> |

The format of datas in `stock-<code>`  

```
(<day2>-<day1>)/<day1>
(<day3>-<day2>)/<day2>

....

```

We use [yFinance](https://pypi.org/project/yfinance) to get the data of them from **Yahoo Finance**.

We plan the output of the network will be a floating point number means the gain/drop of next day.

### Build
---

Define **CC** in Makefile to choose other compiler and chage compile args (default: **GCC**)
use `make` to build release version
```sh
make
```
uss `make debug` to build debugging version
```sh
make debug
```

Tested Environment(s): ArchLinux with GCC 12.1.0
