import twstock
import yfinance
import datetime
import time
import os
import random
def get_twstock(code:str):
    stock=twstock.Stock(code);
    print('Stock : '+stock.sid);
    today=datetime.date.today();
    tyear=int(today.strftime('%Y'));
    tmonth=int(today.strftime('%m'));
    month=1;
    File=open('stock-'+code,'wt');
    lines=0;
    year=2006;
    time.sleep(random.randint(1,60))
    while year<=tyear:
        if month==tmonth and year==tyear:
            break;
        if month>12:
            month=1;
            year+=1;
        stock.fetch(year,month);
        print(str(year)+'-'+str(month)+' : '+str(stock.price));
        for i in range(1,len(stock.price)):
            File.write(str((stock.price[i]-stock.price[i-1])/stock.price[i-1])+'\n');
            lines+=1;
            t=random.randint(5,60)
            print('Sleep for ',t,'secs')
            time.sleep(t);
        month+=1;
    File.close();
    return lines;
def get_yahoo(coder : str):
    op='Open'
    cl='Close'
    code=coder
    #if coder.endswith('.TWO'):
    #    cpde=coder[:len(coder)-4]
    #elif coder.endswith('.TW'):
    #    code=coder[:len(coder)-3]
    stock=yfinance.Ticker(coder)
    his=stock.history(period='max')
    File=open('stock/stock-'+code,'wt')
    print('Stock=',coder)
    print(len(his[op]))
    lines=0
    for i in range(1,len(his[op])):
        a=his[op][i-1]
        b=his[op][i]
        #print(a,b,(b-a)/a)
        if (str(a)=='nan' or str(b)=='nan'):
            continue;
        if b==a or a==0:
            File.write('0.0\n')
        else:
            File.write('{0:.7f}\n'.format((b-a)/(a)))
        lines+=1
    print(lines)
    File.close()
    return lines
'''
code=list()
print('How many stocks u wanna get : ')
n=int(input())
print('input stock code(if in taiwan needs .TW or .TWO): ')
for i in range(n):
    code.append(str(input()))
for i in range(len(code)):
    get_yahoo(code[i])
'''
code=''
if len(os.sys.argv)>1:
    code=os.sys.argv[1]
else:
    code=str(input('input stock code : '))
get_yahoo(code)
