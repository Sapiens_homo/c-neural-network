#include"neural.h"
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<math.h>
#include<string.h>
#include<sys/stat.h>
#include<sys/types.h>
double mul(Neural_unit* in,Neural_unit* out){
	out->val+=out->val**(out->wei+out->size-1);
	out->val+=out->bia;
	for(int i=0;i<out->size-1;i++){
		out->val+=(in+i)->val**(out->wei+i);
	}
	out->val=active(out->val);
	return out->val;
}
double d_random(double a){
	return (double)rand()/(RAND_MAX/a);
}
double active(double a){
	return tanh(a);
}
double sigmoid(double x){
	const double e=2.71828182845904523536;
	return 1/(1+1/pow(e,x));
}
double cost(double a,double b){
	return (a-b)*(a-b)/b;
}
void random_init(Neural_network* net){
	for(int i=0;i<(*net).level;i++){
		for(int j=0;j<*((*net).sizes+i);j++){
			for(int k=0;k<(*(*((*net).units+i)+j)).size;k++){
				*((*(*((*net).units+i)+j)).wei+k)=d_random((*net).level);
			}
			((*(*((*net).units+i)+j)).bia)=d_random((*net).level);
			((*(*((*net).units+i)+j)).val)=d_random((*net).level);
		}
	}
}
void dump(Neural_network* net,const char *path){
	int (*file_destroy)(FILE*)=fclose;
	FILE *WEI;
	FILE *BIA;
	char *buf0=malloc(256);
	char *buf1=malloc(256);
	sprintf(buf0,"./network");
	mkdir(buf0,0777);
	sprintf(buf0,"./network/%s",path);
	mkdir(buf0,0777);
	sprintf(buf0,"./network/%s/wei",path);
	sprintf(buf1,"./network/%s/bia",path);
	mkdir(buf0,0777);
	mkdir(buf1,0777);
	for(int i=0;i<(*net).level;i++){
		for(int j=0;j<*((*net).sizes+i);j++){
			sprintf(buf0,"./network/%s/wei/%d",path,j);
			sprintf(buf1,"./network/%s/bia/%d",path,j);
			WEI=fopen(buf0,"w");
			BIA=fopen(buf1,"w");
			if(!WEI||!BIA){
				printf("ouob cannot open file\n");
				break;
			}
			for(int k=0;k<(*(*((*net).units+i)+j)).size;k++){
				fprintf(WEI,"%lf\n",*((*(*((*net).units+i)+j)).wei+k));
			}
			fprintf(BIA,"%lf\n",((*(*((*net).units+i)+j)).bia));
			file_destroy(WEI);
			file_destroy(BIA);
		}
	}
	free(buf0);
	free(buf1);
}
double load_bia(void *f){
	double t;
	if(!f){
		return EOF;
	}
	fscanf(f,"%lf",&t);
	return t;
}
double load_wei(void *f){
	double t;
	if(!f){
		return EOF;
	}
	fscanf(f,"%lf",&t);
	return t;
}
