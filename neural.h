#ifndef __NEURAL_H__
#define __NEURAL_H__
typedef struct{
	double bia;
	int size;//last level
	double *wei;
	double val;
} Neural_unit;
typedef struct{
	int *sizes;
	int level;
	Neural_unit **units;
} Neural_network;
double mul(Neural_unit*,Neural_unit*);
double d_random(double);
double active(double);
double sigmoid(double);
double cost(double a,double b);
void dump(Neural_network*,const char*);
double load_bia(void*);
double load_wei(void*);
void random_init(Neural_network*);
#endif
