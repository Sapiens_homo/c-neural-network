//69 best and OOP bad
#include "neural.h"
#include "opencl_func.h"
#include<string.h>
#include<stdio.h>
#include<math.h>
#include<time.h>
#pragma GCC target("sse,sse2,sse3,sse4,avx,avx2")
int main(int argc,char **argv){
	void run(Neural_network*);
	double val0=0;
	double val1=0;
	int lvls,size;
	int *sizes;
	Neural_network net;
	char *file_path;
	char t=0;
	double rate;
	int (*file_destroy)(FILE*)=fclose;
	const char *usage="Usage : predict [<args>]\r\nArgs:\r\n\t-h, --help\t\t\tShow this usage\r\n\t-c, --stock <stock code>\tStock code\r\n\t-l, --level <number>\t\tSet levels\r\n\t-s, --size <number>\t\tNeurons of each level\r\n\t-t, --train\t\t\tTrain the network\n\t-i, --input <a> <b>\t\tNetwork Input(cannot be used with -t)\n\t-p, --percent <n>\t\tFitness(%)";
	if(argc-1){	
		for(int i=1;i<argc;i++){
			if(i+1<argc){
				char *tg=*(argv+i);
				char **tb=argv+i;
				if(!strcmp(*(argv+i),"--help")||!strcmp(*(argv+i),"-h")){
					printf("%s\r\n",usage);
					goto exit;
				}
				if(( !strcmp(tg,"-c") || !strcmp(tg,"--stock"))){
					file_path=*(tb+1);
					i++;
					continue;
				}
				if( !strcmp(tg,"-l")||!strcmp(tg,"--level")){
					lvls=atoi(*(tb+1));
					i++;
					continue;
				}
				if(!strcmp(tg,"-s")||!strcmp(tg,"--size")){
					size=atoi(*(tb+1));
					i++;
					continue;
				}
				if(!strcmp(tg,"-t")||!strcmp(tg,"--train")){
					t=1;
					continue;
				}
				if(!strcmp(tg,"-i")||!strcmp(tg,"--input")){
					if(i+2==argc){
						printf("%s",usage);
						goto exit;
					}
					i++;
					val0=strtod(*(tb+1),NULL);
					i++;
					val1=strtod(*(tb+2),NULL);
					continue;
				}
				if(!strcmp(tg,"-p")||!strcmp(tg,"--percent")){
					i++;
					rate=fmod(fabs(strtod(*(tb+1),NULL)),101.0);
					continue;
				}
			}
		}
	}else{
		printf("%s\n",usage);
		goto exit;
	}
	//debug
	rate/=100;
	printf("lvls %d\nsize %d\nval0 %lf\nval1 %lf\nrate %lf\ntrain? %d\nstock %s\n",lvls,size,val0,val1,rate,t,file_path);
	srand(time(NULL));

	//
	lvls+=2;
	size+=1;
	sizes=malloc(sizeof(*sizes)*lvls);
	*sizes=3;	
	for(int i=1;i<lvls-1;i++){
		*(sizes+i)=size;
	}
	*(sizes+lvls-1)=1;
	//init
	net.level=lvls;
	net.sizes=sizes;
	net.units=malloc(sizeof(*(*(net.units)))*lvls);
	for(int i=0;i<net.level;i++){
		*(net.units+i)=malloc(sizeof(*(*(net.units+i)))**(net.sizes+i));
		for(int j=0;j<*(net.sizes+i);j++){
			(*(net.units+i)+j)->wei=malloc(sizeof(*((*(net.units+i)+j)->wei))*(*(*(net.units+i)+j)).size);
		}
	}
	if(!t){
		FILE *f;
		FILE *g;
		char* buf0=malloc(256);
		char* buf1=malloc(256);
		(*(net.units)+0)->val=val0;
		(*(net.units)+1)->val=val1;
		(*(net.units)+2)->val=0;
		for(int i=1;i<net.level;i++){
			for(int j=0;j<*(net.sizes+i);j++){
				sprintf(buf0,"./network/%s/wei/%d",file_path,j);
				sprintf(buf1,"./network/%s/bia/%d",file_path,j);
				f=fopen(buf0,"r");
				g=fopen(buf1,"r");
				if(f&&g){
					for(int k=0;k<(*(*(net.units+i)+j)).size;k++){
						*((*(*(net.units+i)+j)).wei+k)=load_wei(f);
						(*(*(net.units+i)+j)).bia=load_bia(f);
					}
					file_destroy(f);
					file_destroy(g);
				}else{
					printf("ouob cannot open file\n");
				}
			}
		}
		free(buf0);
		free(buf1);
		run(&net);
		printf("%lf\n",(*(*(net.units+net.level-1))).val);
	}else{
		double avg_cost=1;
		int test_data_amount=0;
		char *buf=malloc(30);
		long long times=1;
		sprintf(buf,"./stock/stock-%s",file_path);
		do{
			FILE *F;
			double goal;
			double tmp;
			double a;
			double b;
			F=fopen(buf,"r");
			test_data_amount=0;
			avg_cost=0;
			fscanf(F,"%lf%lf",&a,&b);
			(*(*(net.units)+2)).val=0;
			if(F){
				printf("training #%lld time(s)\n",times);
				random_init(&net);
				while(fscanf(F,"%lf",&tmp)==1&&tmp!=EOF){
					test_data_amount++;
					(*(*(net.units))).val=a;
					(*(*(net.units)+1)).val=b;
					goal=tmp;
					run(&net);
					a=b;
					b=goal;
					avg_cost+=cost(goal,(*(*(net.units+net.level-1))).val);
				}
				file_destroy(F);
			}else{
				printf("ouob cannot open file\n");
			}
			times++;
			avg_cost/=test_data_amount;
			printf("%d %lf\n",test_data_amount,avg_cost);
		}while(fabs(avg_cost-(rate))>1e-2);
		dump(&net,file_path);
		free(buf);
	}

	//destroy
	for(int i=0;i<net.level;i++){
		for(int j=0;j<*(net.sizes+i);j++){
			free((*(*(net.units+i)+j)).wei);
		}
		free(*(net.units+i));
	}
	free(sizes);
	net.sizes=NULL;
	file_path=NULL;
exit:
	return 0;
}
void run(Neural_network* net){
	for(int i=1;i<(*net).level;i++){
		for(int j=0;j<*((*net).sizes+i);j++){
			//printf("Running level #%d Unit #%d\n",i,j);
			mul(*((*net).units+i-1),((*((*net).units+i))+j));
		}
	}
	(*(*((*net).units)+2)).val=(*(*((*net).units+(*net).level-1))).val;
}
