(CC)=gcc -march=native -mtune=native
all:stock-predict
debug:stock-predict.dbg
neural.o:neural.c
	$(CC) neural.c -c -o neural.o
neural.dbg.o:neural.c
	$(CC) -g neural.c -c -o neural.dbg.o
opencl_func.o:opencl_func.c
	$(CC) opencl_func.c -c -o opencl_func.o
opencl_func.dbg.o:opencl_func.c
	$(CC) -g opencl_func.c -c -o opencl_func.dbg.o
stock-predict:main.c neural.o opencl_func.o
	$(CC) main.c neural.o opencl_func.o -o stock-predict -lm -lOpenCL
stock-predict.dbg:main.c neural.dbg.o opencl_func.dbg.o
	$(CC) -g main.c neural.dbg.o opencl_func.dbg.o -o stock-predict.dbg -lm -lOpenCL
clean:
	-rm -rf *.o
	-rm -rf network
	-rm stock-predict*
